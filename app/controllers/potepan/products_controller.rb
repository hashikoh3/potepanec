class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_GET_MAX = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:default_price, :images]).
      limit(RELATED_PRODUCTS_GET_MAX)
  end

  def search
    @search_word = params[:searchWord]
    searched_products = Spree::Product.ransack(name_or_description_cont: params[:searchWord]).
      result.includes(variants_including_master: %i(images prices))
    @products = searched_products
  end
end
