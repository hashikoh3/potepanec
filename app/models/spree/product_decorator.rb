module Spree::ProductDecorator
  Spree::Product.class_eval do
    def related_products
      Spree::Product.in_taxons(taxons).includes(master: [:default_price, :images]).
        distinct.where.not(id: id)
    end

    scope :filter_by_color, -> (color) {
                              joins(variants: :option_values).
                                where(spree_option_values: { name: color }).distinct
                            }
    scope :filter_by_size, -> (size) {
                             joins(variants: :option_values).
                               where(spree_option_values: { name: size }).distinct
                           }
  end

  Spree::Product.prepend self
end
