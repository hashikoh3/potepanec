require 'rails_helper'

RSpec.describe 'ProductsRequest', type: :request do
  describe 'GET #index' do
    t = Time.current
    let!(:product) do
      create(:product, name: 'RUBY ON RAILS TOTE',
                       price: 0.1599e2, available_on: t)
    end
    let!(:product_1) { create(:product, available_on: t - (60 * 60 * 1)) }
    let!(:product_2) { create(:product, available_on: t - (60 * 60 * 2)) }

    before do
      get potepan_index_path
    end

    it '@new_arrival_productsに正しく値が渡されていること' do
      expect(assigns(:new_products)).to match_array [product, product_1, product_2]
      expect(assigns(:new_products)[0]).to eq product
      expect(assigns(:new_products)[1]).to eq product_1
      expect(assigns(:new_products)[2]).to eq product_2
    end

    it 'トップページに新着商品名が表示されていること' do
      expect(response.body).to include 'RUBY ON RAILS TOTE'
    end

    it 'トップページに新着商品の金額が表示されていること' do
      expect(response.body).to include '$15.99'
    end
  end

  describe 'GET #show' do
    i = Potepan::ProductsController::RELATED_PRODUCTS_GET_MAX - 1
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, i, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    context '関連商品の数が MAX_NUMBER_OF_DISPLAYS の場合' do
      it 'リクエストが成功しているかどうか' do
        expect(response.status).to eq 200
      end

      it '@productに正しく値が渡されていること' do
        expect(assigns(:product)).to eq product
      end

      it '@related_productsに正しく値が渡されていること' do
        expect(assigns(:related_products)).to match_array [related_product, *related_products]
      end
    end

    context '関連商品の数が RELATED_PRODUCTS_GET_MAX +1の場合' do
      it '@related_productsに渡される値の最大数が RELATED_PRODUCTS_GET_MAX であること' do
        create(:product, taxons: [taxon])
        eq Potepan::ProductsController::RELATED_PRODUCTS_GET_MAX
      end
    end

    describe '商品詳細ページが表示されているかどうか' do
      it '商品名が表示されていること' do
        expect(response.body).to include product.name
      end

      it '商品の金額が表示されていること' do
        expect(response.body).to include product.display_price.to_s
      end

      it '商品の説明が表示されていること' do
        expect(response.body).to include product.description
      end
    end

    describe '商品詳細ページに関連商品が表示されていること' do
      it '関連商品の商品名が表示されていること' do
        expect(response.body).to include related_product.name
      end

      it '関連商品の金額が表示されていること' do
        expect(response.body).to include related_product.display_price.to_s
      end
    end
  end
end
