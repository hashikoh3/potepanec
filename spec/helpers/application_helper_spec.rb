require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    context "引数に文字列が与えられなかった時" do
      it "'BIGBAG Store'という文字列のみを返す" do
        expect(full_title('')).to eq "BIGBAG Store"
      end
    end

    context "文字列に引数が与えられた時" do
      it "'BIGBAG Store'という文字列の前に与えられた文字列を追加したものを返す" do
        expect(full_title('テスト')).to eq "テスト - BIGBAG Store"
      end
    end
  end
end
